#!/usr/bin/env bash
#
# LICENSE:
# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org>

EVERY=10
FPS=30
ITERATIONS=0
LOG="y"
OUT="./out.webm"
QUALITY=75
VIDEO="y"

# If any arguments were given, check what they are and act appropriately.
if [[ $# -ne 0 ]]; then
    # Print the help if requested.
    if [[ "$1" = "-h" ]] || [[ "$1" = "--help" ]]; then
        echo -e "Usage: $ ./workcapture.sh [OPTIONS]\n"
        echo "  --help        Display this help"
        echo "  --every       The number of seconds between each screenshot; 1+"
        echo "  --fps         The frames per second of the resultant video, if"
        echo "                one is being made."
        echo "  --iterations  The number of iterations to perform before"
        echo "  --log         Whether or not to log recordings to the tty [Y/n]"
        echo "  --out         Destination for the resultant video if enabled."
        echo "  --quality     The percentage quality of the screenshots [1-100]"
        echo "                stopping. Defaults to infinite."
        echo "  --video       Whether or not to make a video of the"
        echo "                resultant screenshots. [Y/n]"
        echo -e "\n"

        exit 0
    else
        while :; do
            case "$1" in
                --every)      EVERY="$2";      shift 2;;
                --fps)        FPS="$2";        shift 2;;
                --iterations) ITERATIONS="$2"; shift 2;;
                --log)        LOG="$2";        shift 2;;
                --out)        OUT="$2";        shift 2;;
                --quality)    QUALITY="$2";    shift 2;;
                --video)      VIDEO="$2";      shift 2;;
                --)           shift; break;;
                *)            shift; break;;
            esac
        done
    fi
fi

# Ensure $EVERY is at least 1.
if (( $EVERY < 1 )); then
    EVERY=1
fi

ITERATIONS_PASSED=0

# Loop forever.
while :; do
    # Take a screenshot of the currently focused window.
    $(scrot --focused --quality "$QUALITY")

    # Increment the number of iterations passed.
    ITERATIONS_PASSED=$((ITERATIONS_PASSED+1))

    DATE=$(date -d "@$(date +%s)" +"%H-%M-%S")
    echo "[${DATE}] Recorded."

    # If the number of iterations passed is equal to the total number set to
    # perform, then break out.
    if [[ "$ITERATIONS_PASSED" -eq "$ITERATIONS" ]]; then
        echo "[${DATE}] Iterations met."

        break
    fi

    # Check if the 'q' is typed in terminal. If so, break out now.
    read -t "$EVERY" -n 1 KEY

    if [[ "$KEY" = "q" ]]; then
        break
    fi
done

# Create a video of their screenshots if they'd like.
if [[ "$VIDEO" = "y" ]] || [[ "$VIDEO" = "yes" ]]; then
    echo "Creating video to: $OUT"

    ffmpeg -framerate 30 -vb 20M -pattern_type glob -i '*.png' "$OUT"
fi
