[![license-badge][]][license]

# workcapture.sh

Captures the contents of the currently focused window every X amount of seconds
(defaults to 10) for a total number of iterations (defaults to infinite).

### Installation

Requires `scrot` to be installed. On Arch, this is the `scrot` package.

```
make install
```

### Examples

```
./workcapture.sh --every 10 --iterations 100
# Captures the focused window at the time every 10 seconds, 100 times.
```

### Commands

```
./workcapture.sh --help
```

[license-badge]: https://img.shields.io/badge/license-UNLICENSE-lightgrey.svg?style=flat-square
[license]: http://unlicense.org/
